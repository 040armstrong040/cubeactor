// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyCubesGameMode.generated.h"

UCLASS(minimalapi)
class AMyCubesGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyCubesGameMode();
};



