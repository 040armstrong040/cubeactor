// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyCubesGameMode.h"
#include "MyCubesHUD.h"
#include "MyCubesCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyCubesGameMode::AMyCubesGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyCubesHUD::StaticClass();
}
